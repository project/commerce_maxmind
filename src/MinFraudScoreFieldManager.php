<?php

namespace Drupal\commerce_maxmind;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;

/**
 * Manages minfraud_score fields.
 *
 * MinFraud score fields are decimal fields storing values of a minfraud score
 * on orders.
 */
class MinFraudScoreFieldManager {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public $entityTypeManager;

  /**
   * Local cache for minfraud_score field definitions.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  protected $fieldDefinitions = [];

  /**
   * Name of the MinFraud score field on order entities.
   */
  const FIELD_NAME = 'minfraud_score';

  /**
   * Constructs a new MinFraudScoreFieldManager object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Gets a minfraud_score field definition.
   *
   * @param string $order_type_id
   *   The order type ID.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   *   The minfraud_score field definition.
   */
  public function getFieldDefinition($order_type_id) {
    if (!isset($this->fieldDefinitions[$order_type_id])) {
      $definitions = $this->entityFieldManager->getFieldDefinitions('commerce_order', $order_type_id);
      $this->fieldDefinitions[$order_type_id] = $definitions[self::FIELD_NAME] ?? NULL;
    }
    return $this->fieldDefinitions[$order_type_id];
  }

  /**
   * Clears the minfraud_score definition cache.
   */
  public function clearCaches() {
    $this->fieldDefinitions = [];
  }

  /**
   * Creates a minfraud_score field.
   *
   * @param string $order_type_id
   *   The order type ID.
   */
  public function createField($order_type_id) {
    $field_storage = FieldStorageConfig::loadByName('commerce_order', self::FIELD_NAME);
    $field = FieldConfig::loadByName('commerce_order', $order_type_id, self::FIELD_NAME);
    if (empty($field_storage)) {
      $field_storage = FieldStorageConfig::create([
        'field_name' => self::FIELD_NAME,
        'entity_type' => 'commerce_order',
        'type' => 'decimal',
        'cardinality' => 1,
        'locked' => TRUE,
        'settings' => [
          'precision' => 4,
          'scale' => 2,
        ],
        'translatable' => FALSE,
      ]);
      $field_storage->save();
    }
    if (empty($field)) {
      $field = FieldConfig::create([
        'field_storage' => $field_storage,
        'bundle' => $order_type_id,
        'label' => t('MinFraud score'),
        'required' => FALSE,
        'settings' => [
          'handler' => 'default',
          'handler_settings' => [],
          'min' => 0.1,
          'max' => 99,
        ],
        'translatable' => FALSE,
      ]);
      $field->save();

      /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $form_display */
      $form_display = commerce_get_entity_display('commerce_order', $order_type_id, 'form');
      $form_display->setComponent(self::FIELD_NAME, [
        'type' => 'options_select',
        'weight' => 100,
      ]);
      $form_display->save();

      $this->clearCaches();
    }
  }

  /**
   * Checks whether the minfraud_score field for the given order type.
   *
   * An minfraud_score field is no longer deletable once it has data.
   *
   * @param string $order_type_id
   *   The order type ID.
   *
   * @return bool
   *   TRUE if the minfraud_score field can be deleted, FALSE otherwise.
   */
  public function canDeleteField($order_type_id) {
    $field = FieldConfig::loadByName('commerce_order', $order_type_id, self::FIELD_NAME);
    if (!$field) {
      return FALSE;
    }
    $query = $this->entityTypeManager->getStorage('commerce_order')->getQuery()
      ->condition('type', $order_type_id)
      ->exists(self::FIELD_NAME)
      ->range(0, 1);
    $result = $query->execute();

    return empty($result);
  }

  /**
   * Deletes the minfraud_score field for the given order type.
   *
   * @param string $order_type_id
   *   The order type ID.
   */
  public function deleteField($order_type_id) {
    if (!$this->canDeleteField($order_type_id)) {
      return;
    }

    $field = FieldConfig::loadByName('commerce_order', $order_type_id, self::FIELD_NAME);
    if ($field) {
      $field->delete();
      $this->clearCaches();
    }
  }

}
