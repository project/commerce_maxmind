<?php

namespace Drupal\commerce_maxmind;

use MaxMind\MinFraud;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayInterface;

/**
 * A service to communicate with MaxMind's minFraud API.
 */
class MinFraudClient {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public $entityTypeManager;

  /**
   * The MaxMind MinFraud client instance.
   *
   * @var \MaxMind\MinFraud
   */
  protected $minFraud;

  /**
   * Constructs a MinFraudClient object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $credentials = $config_factory->get('commerce_maxmind.credentials');
    $accountId = $credentials->get('account_id');
    $licenseKey = $credentials->get('license_key');
    $this->minFraud = new MinFraud($accountId, $licenseKey);
  }

  /**
   * Gets minFraud score for an order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return float
   *   The score.
   */
  public function getScore(OrderInterface $order) {
    $amount = $order->getTotalPrice();
    $request = $this->minFraud->withOrder([
      'amount' => $amount->getNumber(),
      'currency' => $amount->getCurrencyCode(),
      // 'discount_code'    => 'FIRST',
      // 'is_gift'          => true,
      // 'has_gift_message' => false,
      // 'affiliate_id'     => 'af12',
      // 'subaffiliate_id'  => 'saf42',
      // 'referrer_uri'     => 'http://www.amazon.com/',
    ]);

    $request = $request->withEvent([
      'type' => 'purchase',
      'transaction_id' => $order->getOrderNumber(),
      'shop_id' => $order->getStoreId(),
      'time' => date('c', $order->getPlacedTime()),
    ]);

    $request = $request->withDevice([
      'ip_address' => $order->getIpAddress(),
      // 'session_age' => 3600.5,
      // 'session_id' => 'foobar',
      // 'user_agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.89 Safari/537.36',
      // 'accept_language' => 'en-US,en;q=0.8',
    ]);

    /** @var \Drupal\user\UserInterface $customer */
    $customer = $order->getCustomer();
    if (!$customer->isAnonymous()) {
      $request = $request->withAccount([
        'user_id' => $customer->id(),
        'username_md5' => md5($customer->getAccountName()),
      ]);
    }

    $email = $order->getEmail();
    $request = $request->withEmail([
      'address' => $email,
      'domain' => substr(strrchr($email, '@'), 1),
    ]);

    foreach ($order->getItems() as $order_item) {
      $request = $request->withShoppingCartItem([
        'item_id' => $order_item->getPurchasedEntity()->getSku(),
        'quantity' => (int) $order_item->getQuantity(),
        'price' => $order_item->getUnitPrice()->getNumber(),
        // 'category' => 'pets',
      ]);
    }

    /** @var \Drupal\profile\Entity\ProfileInterface[] $profiles */
    $profiles = $order->collectProfiles();
    if (isset($profiles['billing'])) {
      /** @var \Drupal\address\AddressInterface $address */
      $address = $profiles['billing']->address->first();
      $request = $request->withBilling([
        'first_name' => $address->getGivenName(),
        'last_name' => $address->getFamilyName(),
        'address' => $address->getAddressLine1(),
        'address_2' => $address->getAddressLine2(),
        'city' => $address->getLocality(),
        'region' => $address->getAdministrativeArea(),
        'country' => $address->getCountryCode(),
        'postal' => $address->getPostalCode(),
        // TODO:
        // 'company' => 'Company',
        // 'phone_number' => '123-456-7890',
        // 'phone_country_code' => '1',
      ]);
    }

    if (isset($profiles['shipping'])) {
      /** @var \Drupal\address\AddressInterface $address */
      $address = $profiles['shipping']->address->first();
      $request = $request->withShipping([
        'first_name' => $address->getGivenName(),
        'last_name' => $address->getFamilyName(),
        'address' => $address->getAddressLine1(),
        'address_2' => $address->getAddressLine2(),
        'city' => $address->getLocality(),
        'region' => $address->getAdministrativeArea(),
        'country' => $address->getCountryCode(),
        'postal' => $address->getPostalCode(),
        // TODO:
        // 'company' => 'Company',
        // 'phone_number' => '123-456-7890',
        // 'phone_country_code' => '1',
        // 'delivery_speed' => 'same_day',
      ]);
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getPayment($order);
    if (!empty($payment)) {
      /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
      $payment_gateway = $payment->getPaymentGateway();
      $processor = self::getPaymentProcessor($payment_gateway->getPlugin());
      if (!empty($processor)) {
        $request = $request->withPayment([
          'processor' => $processor,
          'was_authorized' => self::isPaymentAuthorized($payment),
          // TODO:
          // 'decline_code' => 'invalid number',
        ]);
      }

      /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
      $payment_method = $payment->getPaymentMethod();
      if ($payment_method->id() === 'credit_card') {
        // TODO:
        // $request = $request->withCreditCard([
        //   'issuer_id_number' => '411111',
        //   'last_4_digits' => '7643',
        //   'bank_name' => 'Bank of No Hope',
        //   'bank_phone_country_code' => '1',
        //   'bank_phone_number' => '123-456-1234',
        //   'avs_result' => 'Y',
        //   'cvv_result' => 'N',
        // ]);
      }
    }

    $scoreResponse = $request->score();
    return (float) $scoreResponse->riskScore;
  }

  /**
   * Gets last authorized payment for an order or the most recent failed
   * payment if there's no authorized payment.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   *   The payment.
   */
  public function getPayment(OrderInterface $order) {
    /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface[] $payments */
    $payments = $payment_storage->loadMultipleByOrder($order);

    // TODO: find the most suitable payment. I.e. one that's authorized, or the
    // last failed payment attempt.
    return array_values($payments)[0] ?? NULL;
  }

  /**
   * Checks if the payment was authorized or not.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   *
   * @return bool
   *   TRUE if payment state is 'authorization' or 'completed', FALSE
   *   otherwise.
   */
  public static function isPaymentAuthorized(PaymentInterface $payment) {
    $authorized_states = [
      'authorization',
      'completed',
    ];
    return in_array($payment->getState()->getId(), $authorized_states);
  }

  /**
   * Given a payment method ID returns its corresponding MaxMind payment processor ID.
   *
   * @param Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayInterface $payment_gateway
   *   The payment gateway plugin.
   *
   * @return string
   *   MaxMind payment processor ID.
   */
  public static function getPaymentProcessor(PaymentGatewayInterface $payment_gateway) {
    // TODO: map more payment gateways from Drupal Commerce payment modules.
    $gateways_map = [
      'commerce_authnet' => 'authorizenet',
      'commerce_braintree' => 'braintree',
      'commerce_paypal' => 'paypal',
      'commerce_stripe' => 'stripe',
    ];
    return $gateways_map[$payment_gateway->getPluginDefinition()['provider']] ?? NULL;
  }

}
