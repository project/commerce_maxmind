<?php

namespace Drupal\commerce_maxmind\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce\EntityHelper;
use Drupal\commerce_maxmind\MinFraudScoreFieldManager;

/**
 * Configure Maxmind integration.
 *
 * @internal
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The minFraud score field manager.
   *
   * @var \Drupal\commerce_maxmind\MinFraudScoreFieldManager
   */
  protected $minFraudScoreFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a SiteInformationForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\commerce_maxmind\MinFraudScoreFieldManager $minfraud_score_field_manager
   *   The minFraud score field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MinFraudScoreFieldManager $minfraud_score_field_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->minFraudScoreFieldManager = $minfraud_score_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('commerce_maxmind.minfraud_score_field_manager'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_maxmind_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_maxmind.credentials'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $credentials = $this->config('commerce_maxmind.credentials');
    $accountId = $credentials->get('account_id');
    $licenseKey = $credentials->get('license_key');

    $form['account_id'] = [
      '#type' => 'textfield',
      '#title' => t('Account ID'),
      '#default_value' => $accountId,
      '#required' => TRUE,
    ];
    $form['license_key'] = [
      '#type' => 'textfield',
      '#title' => t('License key'),
      '#default_value' => $licenseKey,
      '#required' => TRUE,
    ];

    $order_type_storage = $this->entityTypeManager->getStorage('commerce_order_type');
    $order_types = $order_type_storage->loadMultiple();

    $form['order_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Order types'),
      '#options' => EntityHelper::extractLabels($order_types),
      '#access' => count($order_types) > 0,
    ];

    foreach ($order_types as $order_type_id => $order_type) {
      if ($this->minFraudScoreFieldManager->getFieldDefinition($order_type_id) !== NULL) {
        $form['order_types']['#default_value'][$order_type_id] = $order_type_id;
        if (!$this->minFraudScoreFieldManager->canDeleteField($order_type_id)) {
          $form['order_types'][$order_type_id] = [
            '#disabled' => TRUE,
          ];
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('commerce_maxmind.credentials')
      ->set('account_id', $form_state->getValue('account_id'))
      ->set('license_key', $form_state->getValue('license_key'))
      ->save();

    $order_types = $form_state->getValue('order_types');
    foreach ($order_types as $order_type_id => $order_type_value) {
      $has_field = (bool) $this->minFraudScoreFieldManager->getFieldDefinition($order_type_id);
      if (empty($order_type_value) && $has_field && $this->minFraudScoreFieldManager->canDeleteField($order_type_id)) {
        $this->minFraudScoreFieldManager->deleteField($order_type_id);
      }
      if (!empty($order_type_value) && !$has_field) {
        $this->minFraudScoreFieldManager->createField($order_type_id);
      }
    }

    parent::submitForm($form, $form_state);
  }

}
