<?php

namespace Drupal\commerce_maxmind\EventSubscriber;

use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\commerce_maxmind\MinFraudClient;
use Drupal\commerce_maxmind\MinFraudScoreFieldManager;

/**
  * Caculates minFraud score when order is being placed.
  */
class OrderPlacedSubscriber implements EventSubscriberInterface {

  /**
    * The minFraud client service.
    *
    * @var Drupal\commerce_maxmind\MinFraudClient
    */
  protected $minFraudClient;

  /**
   * The minFraud score field manager.
   *
   * @var \Drupal\commerce_maxmind\MinFraudScoreFieldManager
   */
  protected $minFraudScoreFieldManager;

  /**
    * Constructs a new OrderPlacedSubscriber object.
    *
    * @param Drupal\commerce_maxmind\MinFraudClient $minfraud_client
    *   The minFraud client service.
    * @param \Drupal\commerce_maxmind\MinFraudScoreFieldManager $minfraud_score_field_manager
    *   The minFraud score field manager.
    */
  public function __construct(MinFraudClient $minfraud_client, MinFraudScoreFieldManager $minfraud_score_field_manager) {
    $this->minFraudClient = $minfraud_client;
    $this->minFraudScoreFieldManager = $minfraud_score_field_manager;
  }

  /**
    * {@inheritdoc}
    */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_order.place.pre_transition' => ['calculateScore'],
    ];
    return $events;
  }

  /**
    * Calculates minFraud score and save it to order.
    *
    * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
    *   The transition event.
    *
    * @todo Should we use queues for scalability?
    */
  public function calculateScore(WorkflowTransitionEvent $event) {
    $order = $event->getEntity();
    $field_definition = $this->minFraudScoreFieldManager->getFieldDefinition($order->bundle());
    if (!empty($field_definition)) {
      $score = $this->minFraudClient->getScore($order);
      $order->set($field_definition->getName(), $score);
    }
  }

}