<?php

namespace Drupal\Tests\commerce_maxmind\Kernel;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\commerce_order\Entity\OrderType;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;

/**
 * Tests the minfraud_score field manager.
 *
 * @coversDefaultClass \Drupal\commerce_maxmind\MinFraudScoreFieldManager
 *
 * @group commerce_maxmind
 */
class MinFraudScoreFieldManagerTest extends CommerceKernelTestBase {

  /**
   * The minfraud_score field manager.
   *
   * @var \Drupal\commerce_maxmind\MinFraudScoreFieldManager
   */
  protected $minFraudScoreFieldManager;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'entity_reference_revisions',
    'state_machine',
    'profile',
    'commerce_order',
    'commerce_maxmind',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('commerce_order');

    $this->minFraudScoreFieldManager = $this->container->get('commerce_maxmind.minfraud_score_field_manager');

    OrderType::create([
      'id' => 'purchase',
      'label' => 'Purchase',
      'workflow' => 'order_default',
    ])->save();
  }

  /**
   * @covers ::getFieldDefinition
   * @covers ::clearCaches
   * @covers ::createField
   * @covers ::canDeleteField
   * @covers ::deleteField
   */
  public function testManager() {
    $this->assertEquals(NULL, $this->minFraudScoreFieldManager->getFieldDefinition('purchase'));

    $this->minFraudScoreFieldManager->createField('purchase');
    $field_definition = $this->minFraudScoreFieldManager->getFieldDefinition('purchase');
    $this->assertTrue($field_definition instanceof FieldDefinitionInterface);

    $this->minFraudScoreFieldManager->deleteField('purchase');
    $this->assertEquals(NULL, $this->minFraudScoreFieldManager->getFieldDefinition('purchase'));
  }

}
