<?php

namespace Drupal\Tests\commerce_maxmind\Kernel;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_price\Price;
use Drupal\Tests\commerce_order\Kernel\OrderKernelTestBase;

/**
 * Tests the OrderPlacedSubscriber.
 *
 * @coversDefaultClass \Drupal\commerce_maxmind\EventSubscriber\OrderPlacedSubscriber
 *
 * @group commerce
 */
class OrderPlacedSubscriberTest extends OrderKernelTestBase {

  /**
   * The minfraud_score field manager.
   *
   * @var \Drupal\commerce_maxmind\MinFraudScoreFieldManager
   */
  protected $minFraudScoreFieldManager;

  /**
   * The sample order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * @var Drupal\commerce_maxmind\MinFraudClient|PHPUnit_Framework_MockObject_MockObject
   */
  private $minfraud_client;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'commerce_maxmind',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->minFraudScoreFieldManager = $this->container->get('commerce_maxmind.minfraud_score_field_manager');
    $this->minFraudScoreFieldManager->createField('default');

    $this->minfraud_client = $this->getMockBuilder('Drupal\commerce_maxmind\MinFraudClient')
      ->disableOriginalConstructor()
      ->getMock();

    $this->container->set('commerce_maxmind.minfraud_client', $this->minfraud_client);

    $order_item = OrderItem::create([
      'type' => 'test',
      'quantity' => 1,
      'unit_price' => new Price('10', 'USD'),
    ]);
    $order_item->save();

    $this->order = Order::create([
      'type' => 'default',
      'store_id' => $this->store,
      'order_items' => [$order_item],
      'state' => 'draft',
      'payment_gateway' => 'onsite',
    ]);
    $this->order->save();
  }

  /**
   * Test that minfraud score is being correctly saved to the order.
   */
  public function testOrderPlacedSubscriber() {
    $score = 2.22;

    $this->minfraud_client->expects($this->any())
      ->method('getScore')
      ->with($this->anything())
      ->willReturn($score);

    $this->order->getState()->applyTransitionById('place');
    $this->order->save();

    $this->assertEquals($score, $this->order->get('minfraud_score')->value);
  }

}
